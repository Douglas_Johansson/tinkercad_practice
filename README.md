# Traffic Light simulation

## Info

This is a simulation of a traffic light stop.
The colors alternate between red yellow green for cars
and between green and yellow for pedestrians.
When the lights have turned back to their original position
A cycle is considered complete and printed out in the console.


![scheme](light_tinkercad.PNG)


### Maintainers
@Douglas_Johansson